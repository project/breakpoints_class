# Breakpoints Class module

## Installation:
1. Enable [Breakpoints](https://www.drupal.org/project/breakpoints) and Breakpoints class modules.
2. Download [Enquire.js](https://github.com/WickyNilliams/enquire.js/) library to `/sites/all/libraries/enquire.js/` directory.
3. (optional) Download [matchMedia.js](https://github.com/paulirish/matchMedia.js/) library to `/sites/all/libraries/matchMedia.js/` directory for better IE compatibility.
4. Add some breakpoints with Breakpoints module.
5. Enjoy your classes!

Also libraries can be added using [library_pack module](https://www.drupal.org/project/library_pack).

## Functionality:
- Allows to add classes to body tag based on Breakpoints module configuration for specific themes.
- Allows to setup classes prefix in module settings. It might be helpful if you have some conflicts with breakpoints and already existing body classes.
- Debug mode for logging into Javascript console all breakpoint changes.
